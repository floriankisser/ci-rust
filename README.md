# Continuous Integration for Rust Projects

## Rust image

Contains cargo and rustup for adding more toolchains, if necessary.

## Build locally

Run as unprivileged user, produces `localhost/rust`.

```sh
buildah unshare ./build.sh
```
