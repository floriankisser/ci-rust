#!/usr/bin/env sh

set -ex
. ./env.sh

base=$CI_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE/ci-base/base
toolbox=$CI_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE/ci-base/toolbox

arch=$1

case $arch in
  ""|amd64)
    arch=amd64
    cputype=x86_64
    ;;
  arm64)
    cputype=aarch64
    ;;
  *)
    >&2 echo "Unknown architecture: $arch"
    exit 1
    ;;
esac

url=https://static.rust-lang.org/rustup/dist/$cputype-unknown-linux-gnu/rustup-init
dnf_cache=$(pwd)/build/cache/dnf

mkdir -p "$dnf_cache"

ctr=$(buildah from --override-arch=$arch "$base")
mnt=$(buildah mount "$ctr")
tools=$(buildah from -v "$mnt":/centos_root -v /dev/null:/centos_root/dev/null -v "$dnf_cache":/centos_root/var/cache/dnf:z "$toolbox")

buildah run "$tools" -- dnf -y --installroot=/centos_root --forcearch=$cputype install gcc
buildah run "$tools" -- curl --proto '=https' --tlsv1.2 -sSf $url | buildah run "$ctr" sh -c "cat > rustup-init"

buildah rm "$tools"

old_path=$(buildah run "$ctr" printenv PATH)

buildah config \
  --env RUSTUP_HOME=/opt/rustup/ \
  --env CARGO_HOME=/opt/cargo/ \
  --env PATH="/opt/cargo/bin:$old_path" \
  --label "name=rust" \
  --label "commit=$CI_COMMIT_SHA" \
  "$ctr"

buildah run "$ctr" -- chmod a+x rustup-init
buildah run "$ctr" -- ./rustup-init -y --no-modify-path --profile minimal
buildah run "$ctr" -- rm rustup-init
buildah run "$ctr" -- rustup component add clippy rustfmt

image=$(buildah commit --rm "$ctr" rust)

if [ "$arch" = "amd64" ]; then
  ctr=$(buildah from "$image")
  mnt=$(buildah mount "$ctr")
  tools=$(buildah from -v "$mnt":/centos_root -v /dev/null:/centos_root/dev/null -v "$dnf_cache":/centos_root/var/cache/dnf:z "$toolbox")

  buildah run "$tools" -- dnf -y --installroot=/centos_root --forcearch=$cputype install \
    --enablerepo=crb \
    mingw64-gcc \
    mingw64-winpthreads-static
  buildah rm "$tools"

  buildah run "$ctr" -- rustup target add \
    ${cputype}-pc-windows-gnu \
    ${cputype}-unknown-linux-musl

  buildah config \
    --label "name=cross" \
    --label "commit=$CI_COMMIT_SHA" \
    "$ctr"

  buildah commit --rm "$ctr" cross
fi
